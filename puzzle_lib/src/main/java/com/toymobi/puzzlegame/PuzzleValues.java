package com.toymobi.puzzlegame;

interface PuzzleValues {

    int SIZE = 18;

    int COLUMN = 6;

    int LINE = 3;

    int[] IMAGES_RES_ID = {
            R.drawable.capa,
            R.drawable.page_1,
            R.drawable.page_2,
            R.drawable.page_3,
            R.drawable.page_4,
            R.drawable.page_5,
            R.drawable.page_6,
            R.drawable.page_7};

    int BACKGROUND_SIZE = IMAGES_RES_ID.length;
//    int BACKGROUND_SIZE_SMALL = 3;

    //int[] IMAGES_RES_ID_SMALL = {R.drawable.page_1, R.drawable.page_2, R.drawable.page_3};
}
