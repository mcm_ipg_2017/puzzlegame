package com.toymobi.puzzlegame;

interface OnPuzzleReorderListener {

    void onReorder(int oldIndex, int newIndex);
}
