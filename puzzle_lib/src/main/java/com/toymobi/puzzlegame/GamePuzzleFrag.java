package com.toymobi.puzzlegame;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.toymobi.recursos.BaseFragment;

public class GamePuzzleFrag extends BaseFragment {

    public static final String FRAGMENT_TAG_GAME_PUZZLE = "FRAGMENT_TAG_GAME_PUZZLE";

    private PuzzleController puzzleController;

    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMusicBackground(R.string.path_sound_game_menu, R.raw.minigame_sound, true);

        createSFX(R.raw.sfx_normal_click);

        createVibrationeedback();
    }


    @Override
    public final View onCreateView(@NonNull final LayoutInflater layoutInflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = layoutInflater.inflate(R.layout.minigame_puzzle_layout, container, false);

        createToolBar(R.id.toolbar_puzzle, R.string.puzzle_game_name_toolbar);

        final ViewGroup rootViewLayout = mainView.findViewById(R.id.minigame_puzzle_main_layout);

        if (rootViewLayout != null) {

            if (puzzleController == null) {
                puzzleController = new PuzzleController(getActivity(), rootViewLayout);
            }
        }

        return mainView;
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu,
                                          @NonNull final MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.menu_game_puzzle, menu);

        // Set an icon in the ActionBar
        itemSound = menu.findItem(R.id.sound_menu);

        setIconSoundMenu();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int itemId = item.getItemId();

        if (puzzleController != null) {

            playFeedBackButtons();

            if (itemId == R.id.picture_option) {

                if (puzzleController != null) {
                    puzzleController.changeLevel();
                }

            } else if (itemId == R.id.back_menu) {

                exitOrBackMenu(startSingleApp);

            } else if (itemId == R.id.sound_menu) {

                changeSoundMenu();

            } else {
                super.onOptionsItemSelected(item);
            }
        }
        return true;
    }

    @Override
    public final void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public void deallocate() {
        super.deallocate();

        if (puzzleController != null) {
            puzzleController.deallocateAll();
            puzzleController = null;
        }


        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }
    }
}