package com.toymobi.puzzlegame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.toymobi.framework.media.SimpleSoundPool;
import com.toymobi.framework.options.GlobalSettings;

class PuzzleGridView extends ViewGroup implements View.OnTouchListener {

    private static final int ANIMATION_TIME = 150;

    private final int column = PuzzleValues.COLUMN;

    private int view_size_width, view_size_height;

    private static final int padding = 1;

    private int dragged = -1;

    private int lastX = -1;

    private int lastY = -1;

    private int lastTarget = -1;

    private List<Integer> newPositionList;

    private OnPuzzleReorderListener onRearrangeListener;

    private SimpleSoundPool sfx;

    public PuzzleGridView(Context context) {
        super(context);
    }

    public PuzzleGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PuzzleGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    PuzzleGridView(final Context context, final int view_size, final int viewheight) {

        super(context);

        this.view_size_width = view_size;

        this.view_size_height = viewheight;

        newPositionList = new ArrayList<>();

        setOnTouchListener(this);

        setChildrenDrawingOrderEnabled(true);

        if (sfx == null) {
            sfx = new SimpleSoundPool(context, R.raw.sfx_classic_click);
        }
    }

    @Override
    public void addView(final View child) {
        super.addView(child);
        newPositionList.add(-1);
    }

    @Override
    public void removeViewAt(final int index) {
        super.removeViewAt(index);
        newPositionList.remove(index);
    }

    @Override
    protected void onLayout(final boolean changed, final int l, final int t, final int r, final int b) {

        Point xy;

        for (int i = 0; i < getChildCount(); i++) {
            if (i != dragged) {
                xy = getCoordinateFromIndex(i);
                getChildAt(i).layout(xy.x, xy.y, xy.x + view_size_width, xy.y + view_size_height);
            }
        }
        invalidate();
    }

    @Override
    protected int getChildDrawingOrder(final int childCount, final int i) {
        if (dragged == -1) {
            return i;
        } else if (i == childCount - 1) {
            return dragged;
        } else if (i >= dragged) {
            return i + 1;
        }
        return i;
    }

    private int getIndexFromCoordinate(final int x, final int y) {

        final int col = getTargetFromCoordinateX(x);

        final int row = getTargetFromCoordinateY(y);

        if (col == -1 || row == -1) {
            return -1;
        }
        final int index = row * column + col;

        if (index >= getChildCount()) {
            return -1;
        }
        return index;
    }

    private int getTargetFromCoordinateX(int target_coordinate) {

        target_coordinate -= padding;

        for (int i = 0; target_coordinate > 0; i++) {
            if (target_coordinate < view_size_width) {
                return i;
            }
            target_coordinate -= (view_size_width + padding);
        }
        return -1;
    }

    private int getTargetFromCoordinateY(int target_coordinate) {

        target_coordinate -= padding;

        for (int i = 0; target_coordinate > 0; i++) {
            if (target_coordinate < view_size_height) {
                return i;
            }
            target_coordinate -= (view_size_height + padding);
        }
        return -1;
    }

    private int getTargetFromCoordinate(final int x, final int y) {
        /*if (getTargetFromCoordinate(y) == -1) {
            return -1;
        }*/

        final int leftPos = getIndexFromCoordinate(x - (view_size_width / 4), y);
        final int rightPos = getIndexFromCoordinate(x + (view_size_width / 4), y);

        if (leftPos == -1 && rightPos == -1) {
            return -1;
        }
        if (leftPos == rightPos) {
            return -1;
        }

        int target = -1;
        if (rightPos > -1) {
            target = rightPos;
        } else if (leftPos > -1) {
            target = leftPos + 1;
        }
        if (dragged < target) {
            return target - 1;
        }
        return target;
    }

    private Point getCoordinateFromIndex(final int index) {
        final int col = index % column;
        final int row = index / column;
        return new Point(padding + (view_size_width + padding) * col, padding + (view_size_height + padding) * row);
    }

    public boolean onTouch(final View view, final MotionEvent event) {

        final int action = event.getAction();

        switch (action & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:

                if (sfx != null && GlobalSettings.soundEnable) {
                    sfx.playSound(R.raw.sfx_classic_click);
                }

                lastX = (int) event.getX();
                lastY = (int) event.getY();

                final int index = getLastIndex();

                if (index != -1) {
                    dragged = index;
                    animateDragged();
                    return true;
                }
                break;

            case MotionEvent.ACTION_MOVE:

                if (dragged != -1) {

                    final int x = (int) event.getX(), y = (int) event.getY();

                    final int l = x - (3 * view_size_width / 4);

                    final int t = y - (3 * view_size_height / 4);

                    final int r = l + (view_size_width * 3 / 2);

                    final int b = t + (view_size_height * 3 / 2);


                    getChildAt(dragged).layout(l, t, r, b);

                    final int target = getTargetFromCoordinate(x, y);
                    if (lastTarget != target) {
                        if (target != -1) {
                            animateGap(target);
                            lastTarget = target;
                        }
                    }
                } else {
                    layout(getLeft(), getTop(), getRight(), getBottom());
                }
                lastX = (int) event.getX();
                lastY = (int) event.getY();
                break;
            case MotionEvent.ACTION_UP:
                if (dragged != -1) {
                    final View v = getChildAt(dragged);
                    if (lastTarget != -1) {
                        reorderChildren();
                    } else {

                        final Point xy = getCoordinateFromIndex(dragged);

                        final int l = xy.x;

                        final int t = xy.y;

                        final int r = l + view_size_width;

                        final int b = t + view_size_height;

                        v.layout(l, t, r, b);
                    }

                    v.clearAnimation();
                    if (v instanceof ImageView) {

                        ImageView viewTemp = (ImageView) v;

                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            //noinspection deprecation
                            setImageViewAlphaOldAPI(viewTemp);
                        } else {
                            setImageViewAlphaNewPI(viewTemp);
                        }
                    }
                    lastTarget = -1;
                    dragged = -1;
                }
                break;
        }
        return dragged != -1;
    }

    private void animateDragged() {

        final View view = getChildAt(dragged);

        final int x = getCoordinateFromIndex(dragged).x + view_size_width / 2, y = getCoordinateFromIndex(dragged).y + view_size_height / 2;

        final int l = x - (3 * view_size_width / 4), t = y - (3 * view_size_height / 4);

        view.layout(l, t, l + (view_size_width * 3 / 2), t + (view_size_height * 3 / 2));

        final AnimationSet animSet = new AnimationSet(true);

        final float pivotX = ((float) view_size_width) * 3 / 4;

        final float pivotY = ((float) view_size_height) * 3 / 4;

        final float fromX = .5f;

        final float fromY = .5f;

        final ScaleAnimation scale = new ScaleAnimation(fromX, 1, fromY, 1, pivotX, pivotY);

        scale.setDuration(ANIMATION_TIME);

        final AlphaAnimation alpha = new AlphaAnimation(1, 0.5f);

        alpha.setDuration(ANIMATION_TIME);

        animSet.addAnimation(scale);
        animSet.addAnimation(alpha);
        animSet.setFillEnabled(true);
        animSet.setFillAfter(true);

        view.clearAnimation();
        view.startAnimation(animSet);
    }

    private void animateGap(final int target) {
        for (int i = 0; i < getChildCount(); i++) {
            final View v = getChildAt(i);
            if (i == dragged) {
                continue;
            }
            int newPos = i;
            if (dragged < target && i >= dragged + 1 && i <= target) {
                newPos--;
            } else if (target < dragged && i >= target && i < dragged) {
                newPos++;
            }

            int oldPos = i;
            if (newPositionList.get(i) != -1) {
                oldPos = newPositionList.get(i);
            }
            if (oldPos == newPos) {
                continue;
            }

            final Point oldXY = getCoordinateFromIndex(oldPos);
            final Point newXY = getCoordinateFromIndex(newPos);
            final Point oldOffset = new Point(oldXY.x - v.getLeft(), oldXY.y
                    - v.getTop());
            final Point newOffset = new Point(newXY.x - v.getLeft(), newXY.y
                    - v.getTop());

            final TranslateAnimation translate = new TranslateAnimation(
                    Animation.ABSOLUTE, oldOffset.x, Animation.ABSOLUTE,
                    newOffset.x, Animation.ABSOLUTE, oldOffset.y,
                    Animation.ABSOLUTE, newOffset.y);
            translate.setDuration(ANIMATION_TIME);
            translate.setFillEnabled(true);
            translate.setFillAfter(true);
            v.clearAnimation();
            v.startAnimation(translate);

            newPositionList.set(i, newPos);
        }
    }

    private void reorderChildren() {
        if (onRearrangeListener != null) {
            onRearrangeListener.onReorder(dragged, lastTarget);
        }

        final List<View> children = new ArrayList<>();

        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).clearAnimation();
            children.add(getChildAt(i));
        }
        removeAllViews();
        while (dragged != lastTarget) {
            if (lastTarget == children.size()) {
                children.add(children.remove(dragged));
                dragged = lastTarget;
            } else if (dragged < lastTarget) {
                Collections.swap(children, dragged, dragged + 1);
                dragged++;
            } else {
                Collections.swap(children, dragged, dragged - 1);
                dragged--;
            }
        }
        for (int i = 0; i < children.size(); i++) {
            newPositionList.set(i, -1);
            addView(children.get(i));
        }
        layout(getLeft(), getTop(), getRight(), getBottom());
    }

    private int getLastIndex() {
        return getIndexFromCoordinate(lastX, lastY);
    }

    public void setOnRearrangeListener(final OnPuzzleReorderListener l) {
        this.onRearrangeListener = l;
    }

    final void deallocate() {
        if (newPositionList != null) {
            newPositionList.clear();
            newPositionList = null;
        }
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void setImageViewAlphaOldAPI(ImageView view) {
        if (view != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            view.setAlpha(255);
        }
    }

    private void setImageViewAlphaNewPI(ImageView view) {
        if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setImageAlpha(255);
        }
    }
}