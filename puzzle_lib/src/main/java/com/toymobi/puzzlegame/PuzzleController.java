package com.toymobi.puzzlegame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.collection.SparseArrayCompat;
import androidx.core.content.res.ResourcesCompat;

import com.toymobi.framework.image.UIComponentUtils;
import com.toymobi.framework.media.SimpleSoundPool;
import com.toymobi.framework.options.GlobalSettings;
import com.toymobi.recursos.EduardoStuff;

class PuzzleController {

    private Random random;

    private PuzzleGridView puzzleGridView;

    private int width;

    private int height;

    private int view_size_width, view_size_height;

    private int index_image = 0;

    private Context context;

    private final List<CharSequence> new_goal = new ArrayList<>(PuzzleValues.SIZE);

    private final List<CharSequence> poem = new ArrayList<>(PuzzleValues.SIZE);

    private final List<CharSequence> result_ok = new ArrayList<>(PuzzleValues.SIZE);

    private SparseArrayCompat<BitmapDrawable> imagesList;

    private SparseArrayCompat<Bitmap> piecesImageList;

    private ViewGroup container;

    private int textSize, textColor;

    private SimpleSoundPool sfx;

    PuzzleController(final Context context, final ViewGroup layout_container) {

        if (context != null && layout_container != null) {

            this.context = context;

            this.container = layout_container;

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    final Resources.Theme theme = context.getTheme();

                    if (theme != null) {
                        textSize = UIComponentUtils.getDimensionPixelSizeByTheme(theme,
                                R.dimen.text_med, 25);

                        final int padding = UIComponentUtils.getDimensionPixelSizeByTheme(theme,
                                R.dimen.margin_puzzle, 8);

                        final int widthTemp = layout_container.getMeasuredWidth() - padding;

                        final int heightTemp = layout_container.getMeasuredHeight() - padding;

                        view_size_width = widthTemp / PuzzleValues.COLUMN;

                        view_size_height = heightTemp / PuzzleValues.LINE;

                        width = view_size_width * PuzzleValues.COLUMN;

                        height = view_size_height * PuzzleValues.LINE;

                        index_image = 0;

                        textColor = ResourcesCompat.getColor(context.getResources(), R.color.SNOW, null);

                        loadSfx();

                        start();

                    }
                }
            }, 100);
        }
    }

    private void start() {

        cleanImagePieces();

        loadBkgImages();

        createBkgImages();

        addDraggableGridView();

        setListeners();

        addPuzzle();
    }

    private void loadBkgImages() {

        if (context != null) {

            final Resources resources = context.getResources();

            if (resources != null) {

                final int imageLength = PuzzleValues.IMAGES_RES_ID.length;

                if (imagesList == null) {
                    imagesList = new SparseArrayCompat<>(imageLength);

                    BitmapDrawable bkg_image_temp;

                    for (int i = 0; i < imageLength; i++) {

                        bkg_image_temp = new BitmapDrawable(
                                resources,
                                UIComponentUtils.decodeSampledBitmapFromResource(
                                        resources,
                                        PuzzleValues.IMAGES_RES_ID[i],
                                        width, height));

                        imagesList.put(i, bkg_image_temp);
                    }
                }
            }
        }
    }

    private void createBkgImages() {

        if (piecesImageList == null) {
            piecesImageList = new SparseArrayCompat<>(PuzzleValues.SIZE);
        }

        Bitmap croc_bitmap;

        final BitmapDrawable temp = imagesList.get(index_image);

        if (temp != null) {

            final Bitmap bitmapTemp = temp.getBitmap();

            for (int i = 0; i < PuzzleValues.SIZE; i++) {

                final int line = i / PuzzleValues.COLUMN;

                final int column = ((i < PuzzleValues.COLUMN) ? i : (i % (line * PuzzleValues.COLUMN)));

                final int x = view_size_width * column;

                final int y = view_size_height * line;

                if (imagesList != null) {

                    croc_bitmap = Bitmap.createBitmap(bitmapTemp, x, y, view_size_width, view_size_height);

                    if (croc_bitmap != null) {
                        piecesImageList.put(i, croc_bitmap);
                    }
                }
            }
        }
    }

    private void addPuzzle() {

        for (int i = 0; i < PuzzleValues.SIZE; i++) {
            new_goal.add(String.valueOf(i));
        }

        if (random == null) {
            random = new Random();
        }

        for (int i = 0; i < PuzzleValues.SIZE; i++) {
            final CharSequence word = new_goal.remove(random.nextInt(new_goal
                    .size()));

            final ImageView view = new ImageView(this.context);

            view.setImageBitmap(drawTextToBitmap(word));

            puzzleGridView.addView(view);

            poem.add(word);

            result_ok.add(String.valueOf(i));
        }
    }

    private void setListeners() {
        puzzleGridView.setOnRearrangeListener(new OnPuzzleReorderListener() {

            @Override
            public void onReorder(final int old_index, final int new_index) {
                final CharSequence word = poem.remove(old_index);
                poem.add(new_index, word);
                for (int i = 0; i < PuzzleValues.SIZE; i++) {
                    final CharSequence value = poem.get(i);
                    final CharSequence result = result_ok.get(i);

                    if (value != result) {
                        i = PuzzleValues.SIZE + 1;
                    } else if (i == PuzzleValues.SIZE - 1) {
                        win();
                    }
                }

            }
        });
    }

    private void addDraggableGridView() {

        if (puzzleGridView != null) {
            puzzleGridView.deallocate();

            puzzleGridView = null;
        }

        puzzleGridView = new PuzzleGridView(this.context, view_size_width, view_size_height);

        container.addView(puzzleGridView);
    }

    private void win() {
        if (GlobalSettings.soundEnable) {
            sfx.playSound(R.raw.sfx_win);
        }

        showMessageWinner();
    }

    private Bitmap drawTextToBitmap(@NonNull final CharSequence textNumber) {
        final float scale = context.getResources().getDisplayMetrics().density;

        final int index = Integer.parseInt(textNumber.toString());

        Bitmap bitmap = piecesImageList.get(index);

        Config bitmapConfig;

        if (bitmap != null) {
            bitmapConfig = bitmap.getConfig();
        } else {
            bitmapConfig = Config.ARGB_8888;
        }

        if (bitmapConfig != null && bitmap != null) {

            bitmap = bitmap.copy(bitmapConfig, true);

            final Canvas canvas = new Canvas(bitmap);

            final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(textColor);
            paint.setTextSize((int) (textSize * scale));
            paint.setTypeface(Typeface.DEFAULT_BOLD);

            // Shadown
            // paint.setShadowLayer(1, 1, 3, textbkgColor);

            final Rect bounds = new Rect();
            paint.setTextAlign(Align.CENTER);

            paint.getTextBounds(textNumber.toString(), 0, textNumber.length(),
                    bounds);

            canvas.drawText(textNumber.toString(), view_size_width >> 1, view_size_height >> 1,
                    paint);

            RectF rect = new RectF(0.0f, 0.0f, view_size_width, view_size_height);

            paint.setStyle(Paint.Style.STROKE);

            canvas.drawRoundRect(rect, 0, 0, paint);
        }

        return bitmap;
    }

    private void cleanImagePieces() {

        if (piecesImageList != null) {

            final int size = piecesImageList.size();

            for (int i = 0; i < size; i++) {
                Bitmap bitmap = piecesImageList.get(i);
                if (bitmap != null) {
                    bitmap.recycle();
                }
            }
            piecesImageList.clear();
        }
    }

    private void cleanImages() {

        if (imagesList != null) {

            final int size = imagesList.size();

            for (int i = 0; i < size; i++) {
                BitmapDrawable bitmap = imagesList.get(i);
                if (bitmap != null) {
                    bitmap.getBitmap().recycle();
                }
            }
            imagesList.clear();
        }
    }

    void deallocateAll() {

        cleanImagePieces();

        cleanImages();


        if (puzzleGridView != null) {
            puzzleGridView.removeAllViews();

            puzzleGridView.deallocate();

            puzzleGridView = null;
        }

        new_goal.clear();

        poem.clear();

        result_ok.clear();

        random = null;

        if (sfx != null) {
            sfx.release();
            sfx = null;
        }
    }

    private void showMessageWinner() {
        if (context != null) {

            final OnClickListener okConfirmation = new OnClickListener() {
                @Override
                public void onClick(final View view) {
                    changeLevel();
                }
            };

            final CharSequence title = context
                    .getText(R.string.congrats_message);

            EduardoStuff.showCongratsDialog(context, title, okConfirmation);
        }
    }

    private void loadSfx() {
        if (context != null) {
            if (sfx == null) {
                sfx = new SimpleSoundPool(context, R.raw.sfx_win);
            }
        }
    }

    final void changeLevel() {
        if (container != null) {
            container.removeView(puzzleGridView);

            new_goal.clear();

            poem.clear();

            result_ok.clear();

            final int size = PuzzleValues.BACKGROUND_SIZE - 1;

            if (index_image < size) {
                index_image++;
            } else {
                index_image = 0;
            }
            start();
        }
    }
}
