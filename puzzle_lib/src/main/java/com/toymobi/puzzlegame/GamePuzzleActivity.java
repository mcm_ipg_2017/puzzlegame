package com.toymobi.puzzlegame;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class GamePuzzleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_layout_puzzle);

        startFragment();
    }

    private void startFragment() {

        GamePuzzleFrag.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();

        GamePuzzleFrag gameMemoryFrag = (GamePuzzleFrag) fragmentManager
                .findFragmentByTag(GamePuzzleFrag.FRAGMENT_TAG_GAME_PUZZLE);

        if (gameMemoryFrag == null) {
            gameMemoryFrag = new GamePuzzleFrag();

            fragmentTransaction.add(R.id.main_layout_fragment,
                    gameMemoryFrag,
                    GamePuzzleFrag.FRAGMENT_TAG_GAME_PUZZLE);

            fragmentTransaction.commit();
        }
    }
}
