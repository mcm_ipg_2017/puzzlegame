package com.toymobi.puzzle_app;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.puzzlegame.GamePuzzleFrag;
import com.toymobi.recursos.BaseFragment;

public class GamePuzzleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(com.toymobi.puzzlegame.R.layout.main_layout_puzzle);

        startFragment();
    }

    private void startFragment() {

        GamePuzzleFrag.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();

        final BaseFragment gameMemoryFrag = new GamePuzzleFrag();

            fragmentTransaction.add(com.toymobi.puzzlegame.R.id.main_layout_fragment,
                    gameMemoryFrag,
                    GamePuzzleFrag.FRAGMENT_TAG_GAME_PUZZLE);

            fragmentTransaction.commit();
    }
}
